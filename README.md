# Parsley Health - Patient Enrollment Form

I took a little longer than the recommended time because I was doing research and playing with different ways of handling forms, such as deciding to either go with Formik or React-Hook-Form, which I ended up going with Formik because I wasn't a big fan of the `Controller` wrapper around my input components.

I also decided to go with React router and utilize `MemoryRouter` to handle the multi step functionality. I wanted to use this approach because this is what I have used in the past at other companies, and I had intentions of using React Router to handle going backwards in the form and fixing errors from the summary page by navigating to the step that needed fixed.

## Getting Started

I ran into an issue while using React v18 and MaterialUI. There are some issue with peer dependencies so until the authors fix that problem, you **must** use the flag `--legacy-peer-deps` while installing.

1. Clone the repository
2. Run scripts

```
npm install --legacy-peer-deps

npm start
```

3. Open http://localhost:3000 in your browser to view the app.

## Wishlist of Todos

If I were to continue with this project, and hadn't ran out of time, I would've:

- Beefed up my testing.
- Handled backwards navigation.
- Navigate to the step that needed fixed from summary page.
- Increased my field validation and use a masker to format some fields such as phone number or date.
