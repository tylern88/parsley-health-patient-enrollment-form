import PatientEnrollmentForm from './PatientForms/PatientEnrollmentForm';

import { CssBaseline } from '@mui/material';
import { PatientEnrollmentProvider } from './hooks/EnrollmentProvider';

function App() {
  return (
    <>
      <CssBaseline />
      <PatientEnrollmentProvider>
        <PatientEnrollmentForm />
      </PatientEnrollmentProvider>
    </>
  );
}

export default App;
