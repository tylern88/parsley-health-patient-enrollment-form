import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import DemographInformation from '.';

const renderTree = (
  <MemoryRouter>
    <DemographInformation />
  </MemoryRouter>
);

describe('<DemographicInformation />', () => {
  it('should render the form successfuly', async () => {
    render(renderTree);

    const demographicInfoForm = await screen.findByTestId('header');
    expect(demographicInfoForm).toBeInTheDocument();
  });
  it('should fail to move to next step if there are errors', async () => {
    const handleNextStep = jest.fn();
    render(renderTree);
    const submitButton = await screen.findByTestId('submit-btn');
    fireEvent.click(submitButton);
    await waitFor(() => {
      expect(handleNextStep).not.toHaveBeenCalled();
    });
  });
});
