import { Button, Grid, Typography } from '@mui/material';
import { Formik } from 'formik';
import { useNavigate } from 'react-router-dom';
import { maritalStatus } from '../../components/consts/maritalStatus';
import { genderOptions } from '../../components/consts/genderOptions';
import { states } from '../../components/consts/states';
import FormInputSelect from '../../components/FormInputSelect';
import FormInputText from '../../components/FormInputText';
import validationSchema from './validationSchema';
import { usePatientEnrollment } from '../../hooks/EnrollmentProvider';

const defaultValues = {
  firstName: '',
  lastName: '',
  gender: '',
  email: '',
  address: '',
  city: '',
  state: '',
  zip: '',
  phone: '',
  dateOfBirth: '',
  maritalStatus: '',
};
const DemographicInformation = () => {
  const { handleNextStep } = usePatientEnrollment();

  const navigate = useNavigate();
  return (
    <Formik
      initialValues={defaultValues}
      onSubmit={(values) => {
        handleNextStep({ demograpicData: values });
        navigate('/conditions');
      }}
      validationSchema={validationSchema}
    >
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Typography variant="h6" gutterBottom data-testid="header">
            Demographic Information
          </Typography>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <FormInputText id="firstName" name="firstName" label="First Name" fullWidth />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormInputText id="lastName" name="lastName" label="Last Name" fullWidth />
            </Grid>
            <Grid item xs={12} sm={3}>
              <FormInputSelect id="gender" name="gender" label="Gender" options={genderOptions} />
            </Grid>
            <Grid item xs={12} sm={3}>
              <FormInputText id="dateOfBirth" name="dateOfBirth" label="Date of Birth" fullWidth />
            </Grid>
            <Grid item xs={12} sm={5}>
              <FormInputText id="email" name="email" label="Email" fullWidth />
            </Grid>

            <Grid item xs={12} sm={4}>
              <FormInputText id="phone" name="phone" label="Phone" fullWidth />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormInputText id="address" name="address" label="Address" fullWidth />
            </Grid>
            <Grid item xs={12} sm={5}>
              <FormInputText id="city" name="city" label="City" fullWidth />
            </Grid>
            <Grid item xs={12} sm={3}>
              <FormInputSelect id="state" name="state" label="State" fullWidth options={states} />
            </Grid>
            <Grid item xs={12} sm={3}>
              <FormInputText id="zip" name="zip" label="Zip" fullWidth />
            </Grid>
            <Grid item xs={12} sm={3}>
              <FormInputSelect id="maritalStatus" name="maritalStatus" label="Marital Status" options={maritalStatus} />
            </Grid>
          </Grid>
          <Button variant="contained" type="submit" sx={{ mt: 3, ml: 1 }} data-testid="submit-btn">
            Next
          </Button>
        </form>
      )}
    </Formik>
  );
};

export default DemographicInformation;
