import * as Yup from 'yup';
import { states } from '../../components/consts/states';
import { maritalStatus } from '../../components/consts/maritalStatus';

export default Yup.object().shape({
  firstName: Yup.string().required('Required'),
  lastName: Yup.string().required('Required'),
  dateOfBirth: Yup.string().required('Required'),
  email: Yup.string().email('Invalid email address').required('Required'),
  phone: Yup.string().required('Required'),
  address: Yup.string().required('Required'),
  city: Yup.string().required('Required'),
  state: Yup.string().oneOf(states, 'Please select a valid state').required('Required'),
  zip: Yup.string()
    .matches(/[0-9]*/, 'Zip code must only contain numbers')
    .required('Required'),
  maritalStatus: Yup.string().oneOf(maritalStatus, 'Invalid marital status selected').required('Required'),
  gender: Yup.string().required('Required'),
});
