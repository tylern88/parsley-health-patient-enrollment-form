import { Box, Button, Grid, ListItem, ListItemText, Typography } from '@mui/material';
import { Formik } from 'formik';
import { useNavigate } from 'react-router-dom';
import healthConditions from '../../components/consts/healthConditions';
import FormInputCheckbox from '../../components/FormInputCheckbox';
import { usePatientEnrollment } from '../../hooks/EnrollmentProvider';

const initialValues = {
  'High blood pressure': false,
  'Cardiac Arrest': false,
  Arrhythmia: false,
  'Coronary heart disease': false,
  IBS: false,
  "Crohn's Disease": false,
  Gallstones: false,
  Depression: false,
  Anxiety: false,
  Stress: false,
  Insomnia: false,
  Cancer: false,
  Diabetes: false,
};

const HealthConditions = () => {
  const { handleNextStep } = usePatientEnrollment();
  const navigate = useNavigate();
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        handleNextStep({ healthConditions: values });
        navigate('/medical-questions');
      }}
    >
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Typography variant="h6" gutterBottom>
            Health Conditions
          </Typography>
          <Typography>Please select any health conditions you have or my be experiencing.</Typography>

          <Grid container spacing={3}>
            {healthConditions.map(({ condition }) => (
              <Grid item xs={12} sm={6} key={condition}>
                <ListItem>
                  <ListItemText primary={condition} />
                  <FormInputCheckbox name={condition} />
                </ListItem>
              </Grid>
            ))}
          </Grid>
          <Button variant="contained" type="submit" sx={{ mt: 3, ml: 1 }}>
            Next
          </Button>
        </form>
      )}
    </Formik>
  );
};

export default HealthConditions;
