import { Button, Grid, Typography } from '@mui/material';
import { Formik } from 'formik';
import { useNavigate } from 'react-router-dom';
import FormInputSelect from '../../components/FormInputSelect';
import FormInputText from '../../components/FormInputText';
import { usePatientEnrollment } from '../../hooks/EnrollmentProvider';
import { medicalQuestions } from './questions';

const initialValues = {
  tabaccoFrequency: '',
  alcoholFrequency: '',
  illicitDrugFrequency: '',
  medications: '',
  medicalAllergies: '',
  medicalReason: '',
};

const MedicalQuestions = () => {
  const { handleNextStep } = usePatientEnrollment();
  const navigate = useNavigate();
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        handleNextStep({ medicalHistory: values });
        navigate('/summary');
      }}
    >
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Typography variant="h6" gutterBottom>
            Medical Questionnaire
          </Typography>
          <Typography gutterBottom>These questions assist us with providing you with the best care.</Typography>
          <Grid container spacing={3}>
            {medicalQuestions.map(({ name, question, options }) => (
              <Grid item xs={12} sm={12} key={name}>
                <Typography>{question}</Typography>
                <FormInputSelect name={name} label="Select" options={options} />
              </Grid>
            ))}
            <Grid item xs={12} sm={12}>
              <Typography variant="h6" gutterBottom>
                Current medications
              </Typography>
              <Typography gutterBottom>
                Please list any medications you are currently taking including non-prescription medications, vitamins
                and supplements.
              </Typography>
              <FormInputText name="medications" label="" multiline rows={4} />
            </Grid>
            <Grid item xs={12} sm={12}>
              <Typography variant="h6" gutterBottom>
                Medication allergies or reactions
              </Typography>
              <Typography gutterBottom>Please list any medication allergies or reactions</Typography>
              <FormInputText name="medicalAllergies" label="" multiline rows={3} />
            </Grid>
            <Grid item xs={12} sm={12}>
              <Typography variant="h6" gutterBottom>
                List any surgeries or hospital stays you have had and their approximate date / year
              </Typography>
              <Typography gutterBottom>Type of surgery or reason for hospitalization</Typography>
              <FormInputText name="medicalReason" label="" multiline rows={3} />
            </Grid>
          </Grid>

          <Button variant="contained" type="submit" sx={{ mt: 3, ml: 1 }}>
            Next
          </Button>
        </form>
      )}
    </Formik>
  );
};

export default MedicalQuestions;
