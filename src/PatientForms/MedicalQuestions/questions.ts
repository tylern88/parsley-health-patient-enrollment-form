export const medicalQuestions = [
  {
    name: 'tabaccoFrequency',
    question: 'Do you smoke any tobacco products',
    options: ['Yes', 'Sometimes', 'Never'],
  },
  {
    name: 'alcoholFrequency',
    question: 'Do you drink alcohol',
    options: ['Yes', 'Occasionally', 'Never'],
  },
  {
    name: 'illicitDrugFrequency',
    question: 'Have you regularly used illicit drugs',
    options: ['Yes', 'No'],
  },
];
