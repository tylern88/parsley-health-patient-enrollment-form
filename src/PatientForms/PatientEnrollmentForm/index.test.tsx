import { render, screen } from '@testing-library/react';
import PatientEnrollmentForm from '.';

describe('<PatientEnrollmentForm />', () => {
  it('should render the component', async () => {
    render(<PatientEnrollmentForm />);
    const header = await screen.findByTestId('patient-enrollment-header');
    expect(header).toBeInTheDocument();
  });
});
