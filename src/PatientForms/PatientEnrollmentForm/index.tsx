import { Container, Paper, Typography } from '@mui/material';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import HealthConditions from '../HealthConditions';
import DemographicInformation from '../DemographicInformation';
import MedicalQuestions from '../MedicalQuestions';
import SummaryPage from '../Summary';

const PatientEnrollmentForm = () => {
  return (
    <Container component="main" maxWidth="md">
      <Paper variant="outlined" sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
        <Typography component="h1" variant="h4" data-testid="patient-enrollment-header">
          Patient Enrollment
        </Typography>

        <MemoryRouter initialEntries={['/']}>
          <Routes>
            <Route path="/" element={<DemographicInformation />} />
            <Route path="/conditions" element={<HealthConditions />} />
            <Route path="/medical-questions" element={<MedicalQuestions />} />
            <Route path="/summary" element={<SummaryPage />} />
          </Routes>
        </MemoryRouter>
      </Paper>
    </Container>
  );
};

export default PatientEnrollmentForm;
