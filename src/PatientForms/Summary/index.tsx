import { Divider, ListItem, ListItemText, Typography } from '@mui/material';
import { PatientFormData, usePatientEnrollment } from '../../hooks/EnrollmentProvider';
import TermsAndConditions from '../TermsAndConditions';

const SummaryPage = () => {
  const { getFormValues } = usePatientEnrollment();

  const {
    demograpicData = {},
    healthConditions = {},
    medicalHistory = {},
  } = (getFormValues() as PatientFormData) ?? {};

  const convertToYesNo = (bool?: boolean) => (bool ? 'Yes' : 'No');
  return (
    <>
      <Typography variant="h6" gutterBottom>
        Does everything look correct?
      </Typography>
      <Divider />
      <Typography variant="h6">Patient Information</Typography>
      {Object.keys(demograpicData as PatientFormData).map((value) => (
        <ListItem key={value} sx={{ py: 1, px: 0 }}>
          <ListItemText primary={value} />
          <Typography variant="body2">{demograpicData?.[value as keyof PatientFormData['demograpicData']]}</Typography>
        </ListItem>
      ))}
      <Divider />
      <Typography variant="h6">Health Conditions</Typography>
      {Object.keys(healthConditions as PatientFormData).map((value) => (
        <ListItem key={value} sx={{ py: 1, px: 0 }}>
          <ListItemText primary={value} />
          <Typography variant="body2">
            {convertToYesNo(healthConditions?.[value as keyof PatientFormData['healthConditions']])}
          </Typography>
        </ListItem>
      ))}
      <Divider />
      <Typography variant="h6">Medical History</Typography>
      {Object.keys(medicalHistory as PatientFormData).map((value) => (
        <ListItem key={value} sx={{ py: 1, px: 0 }}>
          <ListItemText primary={value} />
          <Typography variant="body2">{medicalHistory?.[value as keyof PatientFormData['medicalHistory']]}</Typography>
        </ListItem>
      ))}
      <Divider />
      <TermsAndConditions />
    </>
  );
};

export default SummaryPage;
