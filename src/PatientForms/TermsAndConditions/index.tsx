import { Button, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { Formik } from 'formik';
import FormInputCheckbox from '../../components/FormInputCheckbox';
import { usePatientEnrollment } from '../../hooks/EnrollmentProvider';

const TermsAndConditions = () => {
  const { handleSubmit } = usePatientEnrollment();
  return (
    <Formik initialValues={{ termsAccepted: false }} onSubmit={(value) => handleSubmit(value.termsAccepted)}>
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Typography variant="h6">Terms & Conditions</Typography>
          <Typography variant="body1">
            Nullam quis risus eget urna mollis ornare vel eu leo. Aenean lacinia bibendum nulla sed consectetur. Integer
            posuere erat a ante venenatis dapibus posuere velit aliquet. Etiam porta sem malesuada magna mollis euismod.
            Maecenas sed diam eget risus varius blandit sit amet non magna. Etiam porta sem malesuada magna mollis
            euismod. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel
            scelerisque nisl consectetur et. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vestibulum id
            ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.
          </Typography>
          <Box display="flex" justifyContent="center" alignItems="center">
            <Typography>I Accept</Typography>
            <FormInputCheckbox name="termsAccepted" />
          </Box>
          <Button variant="contained" type="submit" fullWidth>
            Submit
          </Button>
        </form>
      )}
    </Formik>
  );
};

export default TermsAndConditions;
