export interface DemographicData {
  firstName: string;
  lastName: string;
  email: string;
  address: string;
  city: string;
  state: string;
  zip: string;
  phone: string;
  dateOfBirth: string;
  maritalStatus: string;
  gender: string;
}

export interface HealthConditions {
  'High blood pressure': boolean;
  'Cardiac Arrest': boolean;
  Arrhythmia: boolean;
  'Coronary heart disease': boolean;
  IBS: boolean;
  "Crohn's Disease": boolean;
  Gallstones: boolean;
  Depression: boolean;
  Anxiety: boolean;
  Stress: boolean;
  Insomnia: boolean;
  Cancer: boolean;
  Diabetes: boolean;
}

export interface MedicalHistory {
  tabaccoFrequency: string;
  alcoholFrequency: string;
  illicitDrugFrequency: string;
  medications: string;
  medicalAllergies: string;
  medicalReason: string;
}

export type FormData = DemographicData | HealthConditions | MedicalHistory;
