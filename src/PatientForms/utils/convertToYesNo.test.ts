import { convertToYesNo } from './convertToYesNo';

describe('Converts a bool to Yes or No', () => {
  it('should convert false to "No"', () => {
    expect(convertToYesNo(false)).toBe('No');
  });
  it('should convert true to "Yes"', () => {
    expect(convertToYesNo(true)).toBe('Yes');
  });
});
