export const convertToYesNo = (bool?: boolean) => (bool ? 'Yes' : 'No');
