import { Checkbox } from '@mui/material';

import { useField } from 'formik';
interface FormInputCheckboxProps {
  id?: string;
  name: string;
}

const FormInputCheckbox = ({ id, name }: FormInputCheckboxProps) => {
  const [field, , helpers] = useField(name);
  const { value: checked } = field;
  const { setValue } = helpers;

  return <Checkbox id={id || name} {...field} checked={checked} onChange={(e) => setValue(e.target.checked)} />;
};

export default FormInputCheckbox;
