import { FormControl, FormHelperText, InputLabel, MenuItem, Select } from '@mui/material';
import { useField } from 'formik';

type SelectOption = {
  label: string;
  value: string | number;
};

interface FormInpuSelectProps {
  id?: string;
  name: string;
  label: string;
  fullWidth?: boolean;
  options: SelectOption[] | string[];
}

const FormInputSelect = ({ id, name, label, fullWidth = true, options }: FormInpuSelectProps) => {
  const [field, meta, helpers] = useField(name);
  const { value } = field;
  const { setValue } = helpers;

  return (
    <FormControl fullWidth={fullWidth} error={!!meta?.error && !!meta?.touched}>
      <InputLabel id={id || name}>{label}</InputLabel>
      <Select
        id={id || name}
        labelId={id || name}
        {...field}
        label={label}
        value={value ?? ''}
        onChange={(e) => setValue(e.target.value)}
      >
        {options?.map((option) => (
          <MenuItem key={(option as SelectOption)?.value ?? option} value={(option as SelectOption)?.value ?? option}>
            {(option as SelectOption)?.label ?? option}
          </MenuItem>
        ))}
      </Select>
      {!!meta?.error && <FormHelperText>{(meta?.touched && meta?.error) ?? ''}</FormHelperText>}
    </FormControl>
  );
};

export default FormInputSelect;
