import { TextField, TextFieldProps } from '@mui/material';

import { useField } from 'formik';
interface FormInputTextProps {
  id?: string;
  name: string;
  label: string;
  required?: boolean;
  fullWidth?: boolean;
  rows?: number;
  multiline?: boolean;
  variant?: TextFieldProps['variant'];
}

const FormInputText = ({
  id,
  label,
  multiline = false,
  rows,
  fullWidth = true,
  variant = 'outlined',
  name,
}: FormInputTextProps) => {
  const [field, meta] = useField(name);

  return (
    <TextField
      id={id || name}
      label={label}
      rows={rows && rows}
      multiline={multiline}
      fullWidth={fullWidth}
      variant={variant}
      error={!!meta?.error && !!meta?.touched}
      helperText={(meta?.touched && meta?.error) ?? ''}
      {...field}
    />
  );
};

export default FormInputText;
