import { useContext, useState, createContext, ReactNode } from 'react';
import { DemographicData, HealthConditions, MedicalHistory } from '../PatientForms/types';

interface Context {
  getFormValues: () => PatientFormData | undefined;
  handleNextStep: (data: PatientFormData) => void;
  handleSubmit: (termsValue: boolean) => void;
}

export type PatientFormData = {
  demograpicData?: DemographicData;
  healthConditions?: HealthConditions;
  medicalHistory?: MedicalHistory;
};

const EnrollmentContext = createContext({} as Context);

const PatientEnrollmentProvider = ({ children }: { children: ReactNode }) => {
  const [state, setState] = useState<PatientFormData>();

  const handleNextStep = (data: PatientFormData) => {
    setState((prevState) => ({ ...prevState, ...data }));
  };

  const handleSubmit = (termsValue: boolean) => {
    const valuesToSendToServer = {
      ...state,
      termsValue,
    };
    console.log(valuesToSendToServer);
  };

  const getFormValues = () => state;

  const values: Context = {
    getFormValues,
    handleNextStep,
    handleSubmit,
  };
  return <EnrollmentContext.Provider value={values}>{children}</EnrollmentContext.Provider>;
};

const usePatientEnrollment = () => {
  const context = useContext<Context>(EnrollmentContext);
  if (context === undefined) {
    throw new Error('useEnrollment must be used within a EnrollmentProvider');
  }
  return context;
};

export { PatientEnrollmentProvider, usePatientEnrollment };
